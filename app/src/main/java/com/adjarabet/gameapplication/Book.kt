package com.adjarabet.gameapplication

class Book {
    var name : String = ""
    var author : String = ""
    var summary : String = ""
    var pageNumber: Int = 0
    fun printSummary(){
        println(summary)
    }

    override fun toString(): String {
        return name
    }

    override fun equals(other: Any?): Boolean {
        if (other is Book){
            if (author == other.author){
                return true
            }
        }
        return false
    }
}

fun main(){
    var book1 = Book()
    book1.name = "sherlok"
    book1.author = "arthur"
    book1.pageNumber = 500
    book1.summary = "arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur "

    var book2 = Book()
    book2.name = "sherlok"
    book2.author = "arthur"
    book2.pageNumber = 500
    book2.summary = "arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur arthur "

    println(book1 == book2)

}