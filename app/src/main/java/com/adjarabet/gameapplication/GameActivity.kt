package com.adjarabet.gameapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast

class GameActivity : AppCompatActivity() {

    private lateinit var resultText: TextView
    private lateinit var checkText: TextView
    private lateinit var checkedTimes: TextView

    val correctPassword : String = "1234"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password)

        resultText = findViewById(R.id.myPassword)
        checkText = findViewById(R.id.checkTV)
        checkedTimes = findViewById(R.id.checkedTimesTV)

    }

//    #N1
    fun clickedViews(clickedView: View) {
        if (clickedView is TextView){
            var result = resultText.text.toString() // amokitxva textis
            val number = clickedView.text.toString()

            if (result.length >= 4){
                resultText.text = result + ""
            } else {
                resultText.text = result + number
            }

        }
    }

//    #N2 & #N4
    fun correctPassword(password: View){
        if (resultText.text == correctPassword){
            Toast.makeText(this, "Correct Password!", Toast.LENGTH_SHORT).show()
            checkedTimes.text = (checkedTimes.text.toString().toInt() + 1).toString()
        } else {
            Toast.makeText(this, "Wrong Password! \nTry again", Toast.LENGTH_SHORT).show()
            checkedTimes.text = (checkedTimes.text.toString().toInt() + 1).toString()
        }
    }

//    #N3
    fun clearPassword(view: View){
        resultText.text = ""
    }


}